-- No. 1 
SELECT customerName 
FROM customers 
WHERE country = "Philippines";

-- No. 2
SELECT contactLastName, contactFirstName
FROM customers
WHERE customerName = "La Rochelle Gifts";

-- No. 3
SELECT productName, MSRP
FROM products
WHERE productName = "The Titanic";

-- No. 4
SELECT firstName, lastName
FROM employees
WHERE email = "jfirrelli@classicmodelcars.com";

-- No. 5
SELECT customerName
FROM customers
WHERE state IS NULL;

-- No. 6
SELECT firstName, lastName, email
FROM employees
WHERE lastName = "Patterson" AND firstName = "Steve";

-- No. 7
SELECT customerName, country, creditLimit
FROM customers
WHERE country != "USA" AND creditLimit > 3000;

-- No. 8
SELECT customerName
FROM customers
WHERE customerName NOT LIKE "%a%";

-- No. 9
SELECT customerNumber
FROM orders
WHERE comments LIKE "%DHL%";

-- No. 10
SELECT productLine
FROM productlines
WHERE textDescription LIKE "%state of the art%";

-- No. 11
SELECT DISTINCT country
FROM customers;

-- No. 12
SELECT DISTINCT status
FROM orders;

-- No. 13
SELECT customerName, country
FROM customers
WHERE country IN("USA", "France", "Canada");

-- No. 14
SELECT employees.firstName, employees.lastName, offices.city
FROM employees
JOIN offices ON employees.officeCode = offices.officeCode
WHERE offices.city = "Tokyo";

-- No. 15
SELECT customerName
FROM customers
WHERE salesRepEmployeeNumber = 1166;

-- No. 16
SELECT products.productName, customers.customerName
FROM products
JOIN orderdetails ON products.productCode = orderdetails.productCode
JOIN orders ON orderdetails.orderNumber = orders.orderNumber
JOIN customers ON orders.customerNumber = customers.customerNumber
WHERE customers.customerName = "Baane Mini Imports";

-- No. 17

SELECT employees.firstName AS employee_firstName, employees.lastName AS employee_lastName, offices.country AS employee_country, customers.customerName, customers.country AS customer_country
FROM employees
JOIN offices ON employees.officeCode = offices.officeCode
JOIN customers ON customers.country = offices.country;

-- No. 18
SELECT firstName, lastName
FROM employees
WHERE reportsTo = 1056;

-- No. 19
SELECT productName, MAX(MSRP) AS highest_MSRP
FROM products;

-- No. 20
SELECT COUNT(customerName)
FROM customers
WHERE country = "UK";

-- No. 21
SELECT COUNT(products.productLine) 
FROM products
JOIN productlines ON products.productLine = productlines.productLine;

-- No. 22
SELECT COUNT(customers.salesRepEmployeeNumber)
FROM customers
JOIN employees ON customers.salesRepEmployeeNumber = employees.employeeNumber;

-- No. 23
SELECT productName, quantityInStock FROM products WHERE productLine = "Planes" AND quantityInStock < 1000;






/*
	Trial No. 16

SELECT products.productName, customers.customerName
FROM products
JOIN orderdetails ON products.productCode = orderdetails.productCode
JOIN orders ON orderdetails.orderNumber = orders.orderNumber
JOIN customers ON orders.customerNumber = customers.customerNumber
WHERE customers.customerName = "Baane Mini Imports";

*/

/*
	Trial No. 14
SELECT employees.firstName, employees.lastName, offices.city
FROM employees
JOIN offices ON employees.officeCode = offices.officeCode
WHERE offices.city = "Tokyo";

*/